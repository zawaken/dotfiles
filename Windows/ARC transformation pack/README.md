### Arc Customization Pack install instructions

1. Download the latest version of [UltraUXThemePatcher](https://www.syssel.net/hoefs/software_uxtheme.php?lang=en)
2. Run through the installer and it'll patch the necessary files.
3. Place the themes in `c:\Windows\Resources\Themes`, in this case place the files in the 'Arc Themes' folder in the `Themes` directory (These themes are for 1703 but work perfectly on 1803)
4. Install the software you want from my repo (I recommend installing the arc icons)

### Uninstalling
You should uninstall UltraUXThemePatcher whenever a new major version change rolls out (e.g. 1709 to 1803)

To uninstall UltraUXThemePatcher you just need to go to programs in your control panel and uninstall UltraUXThemePatcher

The uninstallation of the rest is mostly just opening up the .EXE files and pressing uninstall.

### This theme pack is not one that I've made, I've uploaded it to github for my own sake.
All credit goes to Neiio(Original Creator) and [Niivu](https://niivu.deviantart.com/)(The one that continued Neiio's work)