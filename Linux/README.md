# zawaken's linux dots

### Main Distro:
#### arch

## dependencies

* firefox (nightly)
* i3 ([i3-gaps](https://github.com/Airblader/i3))
* polybar
* scrot
* maim
* xclip
* rxvt-unicode
* zsh (oh-my-zsh)
* playerctl
* feh
* xrandr
* pactl
* playerctl
* rofi
* [sharenix](https://github.com/Francesco149/sharenix)
* compton
* ranger
* vim (+gvim)
* nvim
* git
* w3m
* [polybar-spotify](https://github.com/Jvanrhijn/polybar-spotify)
* [cwm](https://github.com/weabot/cwm)

## nice git repos/additions

* [osu-wine by diamondburned](https://gitlab.com/osu-wine/osu-wine)
